function filterData2(data2) {
    // variabel untuk menampung hasil
    const result = [];
    // variabel untuk menampung object
    let dataValue = 0
    // variabel untuk menyimpan apabila data tidak ditemukan
    let empty = 0

    for(let i = 0; i<data2.length; i++){
        // mengambil data yang female atau FSW4 dan umurnya harus diatas 30
        if((data2[i].gender === "female" || data2[i].company === "FSW4") && data2[i].age > 30){
            result[dataValue] = data2[i]
            dataValue++
        }
        // if(!result.length){
        //     notFound = "message : data tidak ada/tidak di temukan"
        //     result[empty] = notFound
        // }
    }
    return result 
}

const data = require("../data.js")
// console.log(filterData2(data))
module.exports = filterData2(data)

// gender nya female atau company nya FSW4 dan age nya diatas 30 tahun 