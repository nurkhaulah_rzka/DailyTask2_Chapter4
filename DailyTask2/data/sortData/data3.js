function filterData3(data3) {
    // variabel untuk menampung hasil
    const result = [];
    // variabel untuk menampung object
    let dataValue = 0
    // variabel untuk menyimpan apabila data tidak ditemukan
    let empty = 0

    for(let i = 0; i<data3.length; i++){
        if(data3[i].eyeColor === "blue" && data3[i].age >= 35 && data3[i].age <= 40 && data3[i].favoriteFruit === "apple"){
            result[dataValue] = data3[i]
            dataValue++
        }
        // if(!result.length){
        //     notFound = "message : data tidak ada/tidak di temukan"
        //     result[empty] = notFound
        // }
    }
    return result 
}

const data = require("../data.js")
// console.log(filterData3(data))
module.exports = filterData3(data)

// warna mata biru dan age nya diantara 35 sampai dengan 40, dan favorit buah nya apel